/*
* TeamSpeak 3 demo plugin
*
* Copyright (c) 2008-2017 TeamSpeak Systems GmbH
*/

#ifdef _WIN32
#pragma warning (disable : 4100)  /* Disable Unreferenced parameter warning */
#include <Windows.h>
#endif

#pragma comment(lib, "LogitechLEDLib.lib")
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "teamspeak/public_errors.h"
#include "teamspeak/public_errors_rare.h"
#include "teamspeak/public_definitions.h"
#include "teamspeak/public_rare_definitions.h"
#include "teamspeak/clientlib_publicdefinitions.h"
#include "ts3_functions.h"
#include "plugin.h"
#include "LogitechLEDLib.h"


static struct TS3Functions ts3Functions;

#ifdef _WIN32
#define _strcpy(dest, destSize, src) strcpy_s(dest, destSize, src)
#define snprintf sprintf_s
#else
#define _strcpy(dest, destSize, src) { strncpy(dest, src, destSize-1); (dest)[destSize-1] = '\0'; }
#endif

#define PLUGIN_API_VERSION 22

#define PATH_BUFSIZE 512
#define COMMAND_BUFSIZE 128
#define INFODATA_BUFSIZE 128
#define SERVERINFO_BUFSIZE 256
#define CHANNELINFO_BUFSIZE 512
#define RETURNCODE_BUFSIZE 128
#define RGB_MIN 0
#define RGB_MAX 255
#define RGB_PERCENT_MAX 100
#define ALPHA_MAX 255


static char* pluginID = NULL;


const char* ts3plugin_name() {
	return "Logitech LED";
}

const char* ts3plugin_version() {
	return "0.1";
}

int ts3plugin_apiVersion() {
	return PLUGIN_API_VERSION;
}


const char* ts3plugin_author() {
	return "Kspoosh";
}

const char* ts3plugin_description() {
	return "This plugin controls Logitech G lighting";
}

void ts3plugin_setFunctionPointers(const struct TS3Functions funcs) {
	ts3Functions = funcs;
}

/*
* Custom code called right after loading the plugin. Returns 0 on success, 1 on failure.
* If the function returns 1 on failure, the plugin will be unloaded again.
*/
int ts3plugin_init() {
	char appPath[PATH_BUFSIZE];
	char resourcesPath[PATH_BUFSIZE];
	char configPath[PATH_BUFSIZE];
	char pluginPath[PATH_BUFSIZE];

	/* Your plugin init code here */
	printf("PLUGIN: init\n");
	LogiLedInit();

	/* Example on how to query application, resources and configuration paths from client */
	/* Note: Console client returns empty string for app and resources path */
	ts3Functions.getAppPath(appPath, PATH_BUFSIZE);
	ts3Functions.getResourcesPath(resourcesPath, PATH_BUFSIZE);
	ts3Functions.getConfigPath(configPath, PATH_BUFSIZE);
	ts3Functions.getPluginPath(pluginPath, PATH_BUFSIZE, pluginID);

	printf("PLUGIN: App path: %s\nResources path: %s\nConfig path: %s\nPlugin path: %s\n", appPath, resourcesPath, configPath, pluginPath);

	return 0;  /* 0 = success, 1 = failure, -2 = failure but client will not show a "failed to load" warning */
			   /* -2 is a very special case and should only be used if a plugin displays a dialog (e.g. overlay) asking the user to disable
			   * the plugin again, avoiding the show another dialog by the client telling the user the plugin failed to load.
			   * For normal case, if a plugin really failed to load because of an error, the correct return value is 1. */
}

/* Custom code called right before the plugin is unloaded */
void ts3plugin_shutdown() {
	/* Your plugin cleanup code here */
	printf("PLUGIN: shutdown\n");
	LogiLedShutdown();

	if (pluginID) {
		free(pluginID);
		pluginID = NULL;
	}
}




void FlashKeysIncoming()
{
	int duration = 500;
	int redVal = 0;
	int greenVal = 0;
	int blueVal = 0;
	//GetEffectColorValues(&redVal, &greenVal, &blueVal, true);

	int redFinishVal = 255;
	int greenFinishVal = 0;
	int blueFinishVal = 0;
	//GetEffectColorValues(&redFinishVal, &greenFinishVal, &blueFinishVal, false);
	int loopChecked = 1;
	LogiLedPulseSingleKey(LogiLed::KeyName::F1, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F2, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F3, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F4, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F5, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F6, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F7, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F8, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F9, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F10, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F11, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F12, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
}

void FlashKeysOutgoing()
{
	int duration = 500;
	int redVal = 0;
	int greenVal = 0;
	int blueVal = 0;
	//GetEffectColorValues(&redVal, &greenVal, &blueVal, true);

	int redFinishVal = 0;
	int greenFinishVal = 0;
	int blueFinishVal = 255;
	//GetEffectColorValues(&redFinishVal, &greenFinishVal, &blueFinishVal, false);
	int loopChecked = 1;
	LogiLedPulseSingleKey(LogiLed::KeyName::F1, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F2, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F3, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F4, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F5, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F6, redFinishVal, greenFinishVal, blueFinishVal, redVal, greenVal, blueVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F7, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F8, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F9, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F10, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F11, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
	LogiLedPulseSingleKey(LogiLed::KeyName::F12, redVal, greenVal, blueVal, redFinishVal, greenFinishVal, blueFinishVal, duration, loopChecked);
}


void ts3plugin_onTalkStatusChangeEvent(uint64 serverConnectionHandlerID, int status, int isReceivedWhisper, anyID clientID) {
	/* Demonstrate usage of getClientDisplayName */
	char name[512];
	char returnCode[RETURNCODE_BUFSIZE];
	anyID myID;

	if (ts3Functions.getClientID(serverConnectionHandlerID, &myID) != ERROR_ok) {
		ts3Functions.logMessage("Error querying client ID", LogLevel_ERROR, "Plugin", serverConnectionHandlerID);
		return;
	}

	ts3Functions.createReturnCode(pluginID, returnCode, RETURNCODE_BUFSIZE);

	if (ts3Functions.getClientDisplayName(serverConnectionHandlerID, clientID, name, 512) == ERROR_ok) {
		if (clientID == myID) {
			if (status == STATUS_TALKING) {
				//printf("--> %s starts talking\n", name);
				FlashKeysOutgoing();
			}
			else {
				//printf("--> %s stops talking\n", name);
				LogiLedStopEffects();
			}
		}
		else
		{
			if (status == STATUS_TALKING) {
				//printf("--> %s starts talking\n", name);
				FlashKeysIncoming();
			}
			else {
				//printf("--> %s stops talking\n", name);
				LogiLedStopEffects();
			}
		}
	}
}











/*{
#ifdef _WIN32
static char* result = NULL;  /* Static variable so it's allocated only once 
if (!result) {
	const wchar_t* name = L"LOGITECH G LED";
	if (wcharToUtf8(name, &result) == -1) {  /* Convert name into UTF-8 encoded result 
		result = "LOGITECH G LED";  /* Conversion failed, fallback here 
	}
}
return result;
#else
return "LOGITECH G LED";
#endif
}*/

/*#ifdef _WIN32
/* Helper function to convert wchar_T to Utf-8 encoded strings on Windows 
static int wcharToUtf8(const wchar_t* str, char** result) {
	int outlen = WideCharToMultiByte(CP_UTF8, 0, str, -1, 0, 0, 0, 0);
	*result = (char*)malloc(outlen);
	if (WideCharToMultiByte(CP_UTF8, 0, str, -1, *result, outlen, 0, 0) == 0) {
		*result = NULL;
		return -1;
	}
	return 0;
}
#endif*/